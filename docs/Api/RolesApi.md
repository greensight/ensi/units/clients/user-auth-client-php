# Ensi\AdminAuthClient\RolesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createRole**](RolesApi.md#createRole) | **POST** /roles | Создание объекта типа Role
[**deleteRole**](RolesApi.md#deleteRole) | **DELETE** /roles/{id} | Удаление объекта типа Role
[**getRole**](RolesApi.md#getRole) | **GET** /roles/{id} | Получение объекта типа Role
[**patchRole**](RolesApi.md#patchRole) | **PATCH** /roles/{id} | Обновления части полей объекта типа Role
[**searchRoles**](RolesApi.md#searchRoles) | **POST** /roles:search | Поиск объектов типа Role



## createRole

> \Ensi\AdminAuthClient\Dto\RoleResponse createRole($create_role_request)

Создание объекта типа Role

Создание объекта типа Role

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\AdminAuthClient\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_role_request = new \Ensi\AdminAuthClient\Dto\CreateRoleRequest(); // \Ensi\AdminAuthClient\Dto\CreateRoleRequest | 

try {
    $result = $apiInstance->createRole($create_role_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->createRole: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_role_request** | [**\Ensi\AdminAuthClient\Dto\CreateRoleRequest**](../Model/CreateRoleRequest.md)|  |

### Return type

[**\Ensi\AdminAuthClient\Dto\RoleResponse**](../Model/RoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteRole

> \Ensi\AdminAuthClient\Dto\EmptyDataResponse deleteRole($id)

Удаление объекта типа Role

Удаление объекта типа Role

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\AdminAuthClient\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteRole($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->deleteRole: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\AdminAuthClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getRole

> \Ensi\AdminAuthClient\Dto\RoleResponse getRole($id)

Получение объекта типа Role

Получение объекта типа Role

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\AdminAuthClient\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getRole($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->getRole: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\AdminAuthClient\Dto\RoleResponse**](../Model/RoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchRole

> \Ensi\AdminAuthClient\Dto\RoleResponse patchRole($id, $patch_role_request)

Обновления части полей объекта типа Role

Обновления части полей объекта типа Role

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\AdminAuthClient\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_role_request = new \Ensi\AdminAuthClient\Dto\PatchRoleRequest(); // \Ensi\AdminAuthClient\Dto\PatchRoleRequest | 

try {
    $result = $apiInstance->patchRole($id, $patch_role_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->patchRole: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_role_request** | [**\Ensi\AdminAuthClient\Dto\PatchRoleRequest**](../Model/PatchRoleRequest.md)|  |

### Return type

[**\Ensi\AdminAuthClient\Dto\RoleResponse**](../Model/RoleResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchRoles

> \Ensi\AdminAuthClient\Dto\SearchRolesResponse searchRoles($search_roles_request)

Поиск объектов типа Role

Поиск объектов типа Role

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\AdminAuthClient\Api\RolesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_roles_request = new \Ensi\AdminAuthClient\Dto\SearchRolesRequest(); // \Ensi\AdminAuthClient\Dto\SearchRolesRequest | 

try {
    $result = $apiInstance->searchRoles($search_roles_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RolesApi->searchRoles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_roles_request** | [**\Ensi\AdminAuthClient\Dto\SearchRolesRequest**](../Model/SearchRolesRequest.md)|  |

### Return type

[**\Ensi\AdminAuthClient\Dto\SearchRolesResponse**](../Model/SearchRolesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

