# # Role

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID роли | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**title** | **string** | Название роли | [optional] 
**active** | **bool** | Активность | [optional] 
**rights_access** | **int[]** | Права доступа | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


