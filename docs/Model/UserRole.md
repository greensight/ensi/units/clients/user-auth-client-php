# # UserRole

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | ID роли | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**title** | **string** | Название роли | 
**active** | **bool** | Активность | 
**rights_access** | **int[]** | Права доступа | 
**expires** | [**\DateTime**](\DateTime.md) | Время жизни роли | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


